resource "aws_iam_role" "this" {
  name               = "${var.owner}-${var.env}-${var.project}"
  path               = "/"
  assume_role_policy = "${file("${var.assume_role_policy}")}"
}