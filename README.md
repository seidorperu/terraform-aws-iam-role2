# AWS Lambda Role module
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Terraform module which creates Role on AWS, compatible Terraform >= v0.12.1 .

These types of resources are supported:

* [Role](https://www.terraform.io/docs/providers/aws/r/iam_role.html)


## Usage

```hcl
module "aws_iam_role" {
    source                 = "git::ssh://git@bbitbucket.org:seidorperu/terraform-aws-iam-role2.git"
    owner                  = "empresa"
    project                = "lambda"
    env                    = "dev"
    assume_role_policy     = "file.json"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| owner | Propietario del proyecto. | map | n/a | yes |
| project | Nombre del proyecto. | map | n/a | yes |
| env | Entorno de despliegue. | map | n/a | yes |
| policy | Archivo Json- Policy | map | n/a | yes |

## Example Template Policy-Json

```hlc
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": [
              "lambda.amazonaws.com",
             "ec2.amazonaws.com"]
        },
        "Action": "sts:AssumeRole",
        "Sid": ""
      }
    ]
}
```
## Outputs

| Name | Description |
|------|-------------|
| arn | El ARN asignado por AWS a esta política.. |
| id | El id de la política.. |
| name | NOmbre de la politica. |


## Authors

Seidor 

## License

Apache 2 Licensed. See LICENSE for full details.